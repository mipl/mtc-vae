import numpy as np
from os import path
from tqdm import tqdm
import tensorflow as tf
from utils import config
from models.mtc_vae import MTC_VAE
from datasets import FullChunkedDataset


def embed_ds(ds):

    units, factors = ([], [])
    pbar = tqdm(total=ds.size, desc='Embedding data points',
                bar_format='{l_bar}{bar}| {elapsed}<{remaining}>')

    while(True):
        
        try:
            x, lengths, factor = ds.next()
        except StopIteration:
            break

        z = model.embed(tf.concat(x, axis=0))

        assert len(z) == len(factor)

        units.append(z)
        factors.append(factor)

        pbar.update(len(x))

    pbar.close()

    return np.concatenate(units), np.concatenate(factors)


if __name__ == '__main__':

    args = config.get_args(requires_checkpoint=True)

    ds = FullChunkedDataset(data_list=args['test_list'],  **args)

    model = MTC_VAE(None, **args)
    model.train = False
    outp = path.join(args['logdir'], 'embed.npz')
    units, factors = embed_ds(ds)
    np.savez(outp, units=units, factors=factors)

    print('Embeddings saved in ' + outp)
