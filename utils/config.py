import yaml
import tensorflow as tf

from os import path, makedirs
from argparse import ArgumentParser


def get_args(requires_checkpoint=False):

    parser = ArgumentParser(
        description='Tensorflow implementation of MTC-VAE')

    group = parser.add_argument_group('Execution')

    group.add_argument('--config', type=str,
                       help='Path to configuration file')
    group.add_argument('--logdir', type=str,
                       help='Where to store the results')
    group.add_argument('--mgrowth', action='store_true',
                       help='Whether to use GPU memory growth.')

    group = parser.add_argument_group('Sensitivity analysis')

    group.add_argument('--train_list', type=str, help='Train list')
    group.add_argument('--test_list', type=str, help='Test list')
    group.add_argument('--pairs_list', type=str, help='Pairs list')
    group.add_argument('--chunk_size', type=int, help='Chunk size')
    group.add_argument('--order', type=int, help='Order of the model')
    group.add_argument('--beta', type=float, help='Beta hyperparameter')
    group.add_argument('--vlambda', type=float, help='Lambda hyperparameter')
    group.add_argument('--learning_rate', type=float, help='Learning rate')

    args = vars(parser.parse_args())

    local_config = path.join(args['logdir'], 'config.yaml')
    if args['config'] is None:
        args['config'] = local_config

    yargs = yaml.load(open(args['config'], 'r'), Loader=yaml.FullLoader)

    if args['train_list'] is not None:
        yargs['train_list'] = args['train_list']

    if args['test_list'] is not None:
        yargs['test_list'] = args['test_list']

    if args['pairs_list'] is not None:
        yargs['pairs_list'] = args['pairs_list']

    if args['chunk_size'] is not None:
        yargs['chunk_shape'][0] = args['chunk_size']

    if args['order'] is not None:
        yargs['order'] = args['order']

    if args['beta'] is not None:
        yargs['loss_weights']['beta'] = args['beta']

    if args['vlambda'] is not None:
        yargs['loss_weights']['vlambda'] = args['vlambda']

    if args['learning_rate'] is not None:
        yargs['learning_rate'] = args['learning_rate']

    makedirs(args['logdir'], exist_ok=True)
    with open(local_config, 'w') as conf_file:
        yaml.dump(yargs, conf_file)

    args = {k: args[k] for k in ['config', 'logdir', 'mgrowth']}
    args.update(yargs)

    checkpoint_path = path.join(args['logdir'], 'checkpoints', 'checkpoint')
    if requires_checkpoint and not path.exists(checkpoint_path):
        raise Exception('Checkpoint not found')

    if args['mgrowth']:
        for device in tf.config.experimental.list_physical_devices('GPU'):
            tf.config.experimental.set_memory_growth(device, True)

    return args
