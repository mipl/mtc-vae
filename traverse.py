import yaml
import imageio
import numpy as np
import tensorflow as tf

from tqdm import tqdm
from os import path, makedirs
from models.mtc_vae import MTC_VAE
from argparse import ArgumentParser
from datasets import ReenactChunkDataset


def decode(z, model):

    zA, zM = tf.split(z, [model.dim_z, model.dim_w], axis=1)
    return model.dec([zA, zM])[1]


def write_traversal(out_file, x):

    n, d, h, w, c = x.shape
    x = tf.transpose(x, [0, 2, 1, 3, 4])
    x = tf.reshape(x, [n * h, d * w, c])
    x = (x.numpy() * 255).astype(np.uint8)

    imageio.imsave(out_file, x)


def traverse(z, z0, indices):

    indices = list(set(indices))
    z_i = z0
    for i in indices:
        z_i = tf.concat([z_i[:, :i], z[:, None, i], z_i[:, i + 1:]], axis=1)

    return z_i


if __name__ == '__main__':

    parser = ArgumentParser(description='SSIM calculation')

    parser.add_argument('--logdir', type=str, help='Where to save results')
    parser.add_argument('--num_steps', type=int, default=10,
                        help='Number of steps in the traversal')
    parser.add_argument('--mgrowth', action='store_true',
                        help='Whether to use GPU memory growth.')

    args = vars(parser.parse_args())
    args['config'] = path.join(args['logdir'], 'config.yaml')

    yargs = yaml.load(open(args['config'], 'r'), Loader=yaml.FullLoader)
    args.update(yargs)

    if args['mgrowth']:
        for device in tf.config.experimental.list_physical_devices('GPU'):
            tf.config.experimental.set_memory_growth(device, True)

    data = ReenactChunkDataset(**args)
    model = MTC_VAE(None, **args)

    model.train = False
    out_path = path.join(args['logdir'], 'traverse')
    makedirs(out_path, exist_ok=True)

    pbar = tqdm(total=data.size, desc='Calculating traversals',
                bar_format='{l_bar}{bar}| {elapsed}<{remaining}>')
    j = 0
    while(True):
        try:
            x, _, y, _ = data.next()

            for i in range(len(x)):

                z = model.embed(tf.stack([x[i][0], y[i][0]]))
                z = tf.linspace(z[0], z[1], num=args['num_steps'])

                full = decode(z, model)

                root = path.join(out_path, '%07d' % (j))
                makedirs(root, exist_ok=True)

                z0 = tf.tile(z[None, 0], [z.shape[0], 1])

                zA = traverse(z, z0, list(range(model.dim_z)))
                full_a = decode(zA, model)

                zM = traverse(z, z0, list(range(model.dim_z,
                                                model.dim_z + model.dim_w)))
                full_m = decode(zM, model)

                write_traversal(path.join(root, 'full.png'), full)
                write_traversal(path.join(root, 'full_content.png'), full_a)
                write_traversal(path.join(root, 'full_motion.png'), full_m)

                for k in range(model.dim_z + model.dim_w):
                    z_kl = traverse(z, z0, [k])
                    x_kl = decode(z_kl, model)
                    write_traversal(
                        path.join(root, 'feature_%04d.png' % (k)), x_kl)

                j += 1
                pbar.update(1)

        except StopIteration:
            break

    pbar.close()
    print('Traversals saved in ' + out_path)
