import os
import yaml
import numpy as np
import tensorflow as tf

from os import path
from utils import io
from glob import glob
from tqdm import tqdm
from argparse import ArgumentParser


if __name__ == '__main__':

    parser = ArgumentParser(description='SSIM calculation')

    parser.add_argument('--logdir', type=str, help='Where to save results')
    parser.add_argument('--list', type=str, help='Pairs list')
    parser.add_argument('--mgrowth', action='store_true',
                        help='Whether to use GPU memory growth.')

    args = parser.parse_args()

    if args.mgrowth:
        for device in tf.config.experimental.list_physical_devices('GPU'):
            tf.config.experimental.set_memory_growth(device, True)

    if args.list is None:
        yargs = yaml.load(open(os.path.join(args.logdir, 'config.yaml'), 'r'),
                          Loader=yaml.FullLoader)
        args.list = yargs['pairs_list']

    reenact_paths = glob(path.join(args.logdir, 'reenact', '*.png'))
    reenact_paths.sort()

    items = io.file2items(args.list)
    items_source, items_driving = np.split(items, 2, axis=1)

    source_paths = items_source[:, 0]
    source_lengths = tf.strings.to_number(items_source[:, 1], tf.int32)
    driving_lengths = tf.strings.to_number(items_driving[:, 1], tf.int32)

    assert len(source_paths) == len(reenact_paths)

    ssim = []

    for i in tqdm(range(len(source_paths)), desc='Computing SSIM'):

        src = tf.image.decode_image(tf.io.read_file(source_paths[i]))
        rtt = tf.image.decode_image(tf.io.read_file(reenact_paths[i]))
        src_len = source_lengths[i]
        drv_len = driving_lengths[i]

        H = int(np.sqrt(src_len))
        W = int(np.ceil(src_len / H))

        src = tf.image.resize(src[None, ...], [H * 64, W * 64], antialias=True)
        src = tf.cast(src[:, :64, :64], rtt.dtype)

        h, w, _ = rtt.shape
        rtt = tf.stack(tf.split(rtt, w // 64, axis=1))
        rtt = tf.concat(tf.split(rtt, h // 64, axis=1), axis=0)[:drv_len]

        ssim.append(tf.image.ssim(src, rtt, max_val=255))

    ssim = tf.reduce_mean(tf.concat(ssim, axis=0)).numpy()
    print('ssim: {}'.format(ssim))

    ''' For batched comparison, it can be saved in a single file
    out_path = path.join(args.logdir, 'metrics')
    os.makedirs(out_path, exist_ok=True)
    with open(path.join(out_path, 'ssim'), 'w') as f:
        f.write(str(ssim))
    '''
